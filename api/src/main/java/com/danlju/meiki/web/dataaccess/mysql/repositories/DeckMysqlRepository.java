package com.danlju.meiki.web.dataaccess.mysql.repositories;

import com.danlju.meiki.core.domain.Deck;
import com.danlju.meiki.core.domain.User;
import com.danlju.meiki.core.repositories.DeckRepository;
import com.danlju.meiki.web.dataaccess.mysql.models.DeckDbModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class DeckMysqlRepository implements DeckRepository {

    private static final Logger logger = LoggerFactory.getLogger(DeckMysqlRepository.class);

    private final DeckCrudRepository crudRepository;

    public DeckMysqlRepository(DeckCrudRepository crudRepository) {
        this.crudRepository = crudRepository;
    }

    @Override
    public List<Deck> findAll(User user) {
        return toDeckList(
                crudRepository.findAll()
        );
    }

    private List<Deck> toDeckList(Iterable<DeckDbModel> all) {

        List<Deck> decks = new ArrayList<>();
        all.forEach(m -> decks.add(MysqlModelMapper.toDomainModel(m)));

        return decks;

    }

    @Override
    public Deck findById(String id) {
        return crudRepository.findById(id).map(MysqlModelMapper::toDomainModel).orElse(null); // TODO:
    }

    @Override
    public Deck create(Deck deck) {

        var model = crudRepository.save(
                MysqlModelMapper.toDbModel(deck)
        );

        var model1 = crudRepository.save(model);

        // TODO: fix
        return toDomainObject(model1);
        //return deck;
    }

    private Deck toDomainObject(DeckDbModel model1) {
        return MysqlModelMapper.toDomainModel(model1);
    }

}
