package com.danlju.meiki.web.dataaccess.mysql.models;

import com.danlju.meiki.core.domain.Card;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;

@Entity
@Table(name = CardScheduleDataDbModel.TABLE_NAME)
public class CardScheduleDataDbModel {

    public static final String TABLE_NAME = "card_schedule_data";

    @Id
    public String id;

    @Column(name = "card_id")
    public Card card;

    public Instant dueDate;

    public CardScheduleDataDbModel() {
    }

    public CardScheduleDataDbModel(String id, Card card, Instant dueDate) {
        this.id = id;
        this.card = card;
        this.dueDate = dueDate;
    }

    @Override
    public String toString() {
        return "CardScheduleDataDbModel{" +
                "id='" + id + '\'' +
                ", cardId='" + card.getId() + '\'' +
                ", dueDate=" + dueDate +
                '}';
    }
}

