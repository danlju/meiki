package com.danlju.meiki.web.dataaccess.mysql.repositories;

import com.danlju.meiki.core.domain.User;
import com.danlju.meiki.core.domain.UserSecurity;
import com.danlju.meiki.core.repositories.UserRepository;
import com.danlju.meiki.web.dataaccess.mysql.models.UserDbModel;

import java.util.Optional;

public class UserMysqlRepository implements UserRepository {

    private final UserCrudRepository crudRepository;

    public UserMysqlRepository(UserCrudRepository crudRepository) {
        this.crudRepository = crudRepository;
    }

    @Override
    public User findById(String id) {
        return toUser(
                crudRepository.findById(id)
        );
    }

    private User toUser(Optional<UserDbModel> byId) {
        return byId.map(u -> {
            return new User(u.id, u.username, new UserSecurity());
        }).orElse(new User("nope", "nope", new UserSecurity())); // TODO
    }

    @Override
    public void save(User user) {
        crudRepository.save(toDbModel(user));
    }

    private UserDbModel toDbModel(User user) {
        return new UserDbModel(user.getId(), user.getUsername());
    }
}
