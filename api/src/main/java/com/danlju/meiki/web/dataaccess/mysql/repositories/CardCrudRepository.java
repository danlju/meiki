package com.danlju.meiki.web.dataaccess.mysql.repositories;

import com.danlju.meiki.web.dataaccess.mysql.models.CardDbModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;

@Repository
public interface CardCrudRepository extends CrudRepository<CardDbModel, String> {

    List<CardDbModel> findByDeckId(String deckId);

    //@Query("SELECT c FROM Card c WHERE c.deckId = :deckId AND c.dueDate > checkDate")
    //void findDueCards(String deckId, Instant checkDate);
}
