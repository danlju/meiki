package com.danlju.meiki.web.dataaccess.mysql.repositories;

import com.danlju.meiki.core.domain.Card;
import com.danlju.meiki.core.domain.CardField;
import com.danlju.meiki.core.domain.IdUtils;
import com.danlju.meiki.core.repositories.CardRepository;
import com.danlju.meiki.web.dataaccess.mysql.models.CardDbModel;
import com.danlju.meiki.web.dataaccess.mysql.models.CardFieldDbModel;
import com.danlju.meiki.web.dataaccess.mysql.models.CardScheduleDataDbModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class CardMysqlRepository implements CardRepository {

    private static final Logger logger = LoggerFactory.getLogger(CardMysqlRepository.class);

    private final CardCrudRepository crudRepository;

    public CardMysqlRepository(CardCrudRepository crudRepository) {
        this.crudRepository = crudRepository;
    }

    @Override
    public Card findById(final String id) {
        return crudRepository.findById(id)
                .map(this::toCard).orElse(new Card("", "", "", ""));
    }

    private Card toCard(CardDbModel dbModel) {
        return new Card(dbModel.id, dbModel.deckId, toCardFieldList(dbModel.frontFields), toCardFieldList(dbModel.backFields));
    }

    private List<CardField> toCardFieldList(Set<CardFieldDbModel> fields) {
        return fields.stream().map(f -> new CardField(f.id, f.cardId, f.name, f.text)).collect(Collectors.toList());
    }

    @Override
    public List<Card> findByDeckId(String deckId) {
        return crudRepository.findByDeckId(deckId).stream().map(this::toCard).collect(Collectors.toList());
    }

    @Override
    public Card saveOrUpdate(Card card) {

        return toCard(
                crudRepository.save(toDbModel(card))
        );
    }

    private CardDbModel toDbModel(Card card) {

        var dbModel = new CardDbModel(card.getId(), card.getDeckId(), toDbFieldList(card.getFrontFields()), toDbFieldList(card.getBackFields()), null);
        dbModel.scheduleData = new CardScheduleDataDbModel(IdUtils.uuid(), new Card(card.getId()), Instant.now());

        logger.info("Saving card: \n" + dbModel);

        return dbModel;
    }

    private Set<CardFieldDbModel> toDbFieldList(List<CardField> fields) {
        return fields.stream().map(f -> new CardFieldDbModel(f.getId(), f.getCardId(), f.getName(), f.getText())).collect(Collectors.toSet());
    }

    @Override
    public List<Card> findDueCards(String deckId) {
        return null;//return crudRepository.findDueCards(deckId, Instant.now());
    }
}
