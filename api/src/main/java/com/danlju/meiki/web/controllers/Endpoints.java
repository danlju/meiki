package com.danlju.meiki.web.controllers;

public class Endpoints {

    // TODO: move to config?
    public static final String BASE_URI = "/api";

    public static final String DECKS_URI = BASE_URI + "/decks";

    public static final String CARDS_URI = BASE_URI + "/cards";

    private Endpoints() {
    }
}
