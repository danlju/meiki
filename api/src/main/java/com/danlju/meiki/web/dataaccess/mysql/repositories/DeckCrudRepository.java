package com.danlju.meiki.web.dataaccess.mysql.repositories;

import com.danlju.meiki.web.dataaccess.mysql.models.DeckDbModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeckCrudRepository extends CrudRepository<DeckDbModel, String> {
}
