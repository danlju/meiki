package com.danlju.meiki.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(exclude= {MongoAutoConfiguration.class, UserDetailsServiceAutoConfiguration.class})
@ComponentScan("com.danlju.meiki")
public class MeikiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MeikiApplication.class, args);
	}
}
