package com.danlju.meiki.web.repositories;

import com.danlju.meiki.core.domain.Card;
import com.danlju.meiki.core.repositories.CardRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class InMemoryCardRepository implements CardRepository {

    private Map<String, Card> cardMap = new HashMap<>();

    @Override
    public Card findById(String id) {
        return cardMap.get(id);
    }

    @Override
    public List<Card> findByDeckId(final String deckId) {
        return cardMap.values().stream().filter(
                c -> c.getDeckId().equals(deckId)
        ).collect(Collectors.toList());
    }

    @Override
    public Card saveOrUpdate(Card card) {
        cardMap.put(card.getId(), card);
        return card;
    }

    @Override
    public List<Card> findDueCards(String deckId) {
        return null;
    }
}