package com.danlju.meiki.web.controllers.deck;

import com.danlju.meiki.application.usecases.findalldecks.FindAllDecksResultModel;

import java.util.stream.Collectors;

public class DeckViewModelHelper {

    public static ListDecksViewModel toViewModel(FindAllDecksResultModel result) {

        var viewModel = new ListDecksViewModel();

        viewModel.decks = result.getDecks().stream().map(
                d -> new ListDecksViewModel.DeckViewModel(d.getId(), d.getName(), d.getCards().size())
        ).collect(Collectors.toList());

        return viewModel;
    }
}
