package com.danlju.meiki.web.config;

import com.danlju.meiki.application.services.*;
import com.danlju.meiki.application.usecases.getreviewqueue.GetReviewQueueUseCase;
import com.danlju.meiki.application.usecases.reviewcard.ReviewCardUseCase;
import com.danlju.meiki.core.domain.*;
import com.danlju.meiki.core.repositories.CardRepository;
import com.danlju.meiki.core.repositories.DeckRepository;
import com.danlju.meiki.core.repositories.UserRepository;
import com.danlju.meiki.application.usecases.createcard.SaveOrUpdateCardUseCase;
import com.danlju.meiki.application.usecases.createdeck.CreateDeckUseCase;
import com.danlju.meiki.application.usecases.findalldecks.FindAllDecksUseCase;
import com.danlju.meiki.application.usecases.getcard.GetCardUseCase;
import com.danlju.meiki.application.usecases.getdeck.ShowDeckUseCase;
import com.danlju.meiki.web.dataaccess.mysql.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

@Configuration
//@ConditionalOnProperty(value = "module.mysql.enabled", havingValue = "true")
public class AppConfig {

    // TODO: remove this
    public static User testUser = new User(IdUtils.uuid(), "daniel", new UserSecurity());

    public static final String id = IdUtils.uuid();

    @Autowired
    UserRepository userRepository;

    @Bean
    public DeckRepository deckRepository(@Autowired DeckCrudRepository deckCrudRepository, @Autowired UserRepository userRepository) {


        DeckRepository deckRepository = new DeckMysqlRepository(deckCrudRepository);

        AppConfig.testUser = new User(IdUtils.uuid(), "daniel", new UserSecurity());
        userRepository.save(testUser);
        Deck deck = new Deck(id);
        deck.setCreatedByUser(testUser);

        deckRepository.create(deck);

        deck.addCard(new Card(id, deck.getId(), "front", "back"));

        deckRepository.create(deck);


        Deck deck1 = deckRepository.findById(id);

        return deckRepository;
    }

    @Bean
    public CardRepository cardRepository(@Autowired CardCrudRepository cardCrudRepository) {
        return new CardMysqlRepository(cardCrudRepository);
    }

    @Bean
    public UserRepository userRepository(@Autowired UserCrudRepository crudRepository) {
        UserRepository userRepository = new UserMysqlRepository(crudRepository);

        // TODO: remove this

        userRepository.save(testUser);

        return userRepository;
    }

    @Bean
    public CreateDeckUseCase createDeckUseCase(
            @Autowired DeckRepository deckRepository,
            @Autowired UserRepository userRepository) {
        return new CreateDeckService(deckRepository, userRepository);
    }

    @Bean
    public FindAllDecksUseCase findAllDecksUseCase(
            @Autowired DeckRepository deckRepository,
            @Autowired UserRepository userRepository) {
        return new FindAllDecksService(deckRepository, userRepository);
    }

    @Bean
    public GetCardUseCase getCardUseCase(
            @Autowired CardRepository cardRepository) {
        return new GetCardService(cardRepository);
    }

    @Bean
    public SaveOrUpdateCardUseCase createCardUseCase(
            @Autowired CardRepository cardRepository) {
        return new SaveOrUpdateCardService(cardRepository);
    }

    @Bean
    public ShowDeckUseCase findDeckUseCase(@Autowired DeckRepository deckRepository) {
        return new ShowDeckService(deckRepository);
    }

    @Bean
    public GetReviewQueueUseCase getReviewQueueUseCase(@Autowired CardRepository cardRepository) {
        return new GetReviewQueueService(cardRepository);
    }

    @Bean
    public ReviewCardUseCase getReviewCardUseCase(@Autowired CardRepository cardRepository) {
        return new ReviewService(cardRepository);
    }

    //@Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedMethods("GET", "POST").allowedOrigins("*");
    }
}
