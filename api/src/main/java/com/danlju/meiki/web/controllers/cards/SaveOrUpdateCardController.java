package com.danlju.meiki.web.controllers.cards;

import com.danlju.meiki.core.domain.IdUtils;
import com.danlju.meiki.application.usecases.createcard.SaveCardModel;
import com.danlju.meiki.application.usecases.createcard.SaveOrUpdateCardUseCase;
import com.danlju.meiki.web.controllers.CardViewModel;
import com.danlju.meiki.web.controllers.Endpoints;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SaveOrUpdateCardController {

    private static final Logger logger = LoggerFactory.getLogger(SaveOrUpdateCardController.class);

    private final SaveOrUpdateCardUseCase saveOrUpdateCardUseCase;

    public SaveOrUpdateCardController(SaveOrUpdateCardUseCase saveOrUpdateCardUseCase) {
        this.saveOrUpdateCardUseCase = saveOrUpdateCardUseCase;
    }

    @PostMapping(Endpoints.CARDS_URI)
    public CardViewModel createCard(@RequestBody CreateOrUpdateRequest requestModel) {

        if (requestModel.deckId == null || requestModel.deckId.isBlank() || requestModel.front == null || requestModel.front.isBlank()) {
            // TODO : Handle
            return new CardViewModel();
        }

        var creationModel = toCreationModel(requestModel);

        try {
            saveOrUpdateCardUseCase.saveOrUpdate(creationModel);
        } catch (Exception e) {
            logger.error("Save Card failed: ", e);
            return CardViewModel.error("Save failed");
        }

        return new CardViewModel(creationModel.getId(), creationModel.getDeckId(),
                List.of(new CardViewModel.CardFieldViewModel("", "", creationModel.getFront())),
                List.of(new CardViewModel.CardFieldViewModel("", "", creationModel.getBack())),
                ""
        );
    }

    @PutMapping(Endpoints.CARDS_URI)
    public CardViewModel updateCard(@RequestBody CreateOrUpdateRequest requestModel) {

        // TODO: better validation
        if (requestModel.id == null || requestModel.id.isBlank() || requestModel.deckId == null || requestModel.deckId.isBlank() || requestModel.front == null || requestModel.front.isBlank()) {
            return CardViewModel.error("invalid request");
        }

        try {
            saveOrUpdateCardUseCase.saveOrUpdate(toCreationModel(requestModel));
        } catch (Exception e) {
            logger.error("Update failed");
        }
        return new CardViewModel(requestModel.id, requestModel.deckId,
                List.of(new CardViewModel.CardFieldViewModel("", "", requestModel.front)),
                List.of(new CardViewModel.CardFieldViewModel("", "", requestModel.back)),
                "");
    }

    private SaveCardModel toCreationModel(CreateOrUpdateRequest form) {

        var id = form.id == null || form.id.isBlank() ? IdUtils.uuid() : form.id;

        return new SaveCardModel(id, form.deckId, form.front, form.back);
    }
}
