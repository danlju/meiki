package com.danlju.meiki.web.controllers.deck;

import java.util.ArrayList;
import java.util.List;

public class ListDecksViewModel {

    public List<DeckViewModel> decks = new ArrayList<>();

    public static class DeckViewModel {
        
        public String id;
        public String name;
        public int cardCount = 0;

        public DeckViewModel(String id, String name) {
            this.id = id;
            this.name = name;
        }

        public DeckViewModel(String id, String name, int cardCount) {
            this.id = id;
            this.name = name;
            this.cardCount = cardCount;
        }
    }
}
