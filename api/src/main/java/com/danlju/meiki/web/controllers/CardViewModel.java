package com.danlju.meiki.web.controllers;

import java.util.List;

public class CardViewModel {

    public String id;
    public String deckId;
    public List<CardFieldViewModel> frontFields;
    public List<CardFieldViewModel> backFields;
    public String errorMessage = "";


    public CardViewModel() {
    }

    public CardViewModel(String id, String deckId, List<CardFieldViewModel> frontFields, List<CardFieldViewModel> backFields, String errorMessage) {
        this.id = id;
        this.deckId = deckId;
        this.frontFields = frontFields;
        this.backFields = backFields;
        this.errorMessage = errorMessage;
    }

    public static CardViewModel error(final String errorMessage) {

        var model = new CardViewModel();
        model.errorMessage = errorMessage;
        return model;
    }

    public static class CardFieldViewModel {
        public String id;
        public String name;
        public String text;

        public CardFieldViewModel(String id, String name, String text) {
            this.id = id;
            this.name = name;
            this.text = text;
        }
    }
}
