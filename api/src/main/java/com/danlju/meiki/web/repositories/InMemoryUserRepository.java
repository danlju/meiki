package com.danlju.meiki.web.repositories;

import com.danlju.meiki.core.domain.User;
import com.danlju.meiki.core.repositories.UserRepository;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class InMemoryUserRepository implements UserRepository {

    private Map<String, User> userMap = new HashMap<>();

    @Override
    public User findById(String id) {
        return userMap.get(id);
    }

    @Override
    public void save(User user) {
        userMap.put(user.getId(), user);
    }
}
