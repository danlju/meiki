package com.danlju.meiki.web.dataaccess.mysql.models;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = DeckDbModel.TABLE_NAME)
public class DeckDbModel {

    public static final String TABLE_NAME = "deck";

    @Id
    public String id;

    public String name;

    @OneToMany(
            cascade = CascadeType.ALL,
            fetch = FetchType.EAGER,
            mappedBy = "deckId"
    )
    public Set<CardDbModel> cards;

    @ManyToOne
    public UserDbModel user;

    public DeckDbModel() {
    }

    public DeckDbModel(String id, String name, Set<CardDbModel> cards, UserDbModel user) {
        this.id = id;
        this.name = name;
        this.cards = cards;
        this.user = user;
    }

    public DeckDbModel(String deckId) {
        this.id = deckId;
    }

    @Override
    public String toString() {
        return "DeckDbModel{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", cards=" + cards +
                ", user=" + user +
                '}';
    }
}
