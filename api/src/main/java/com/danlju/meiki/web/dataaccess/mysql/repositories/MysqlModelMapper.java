package com.danlju.meiki.web.dataaccess.mysql.repositories;

import com.danlju.meiki.core.domain.*;
import com.danlju.meiki.web.dataaccess.mysql.models.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class MysqlModelMapper {

    private static final Logger logger = LoggerFactory.getLogger(MysqlModelMapper.class);

    public static DeckDbModel toDbModel(Deck deck) {
        return new DeckDbModel(deck.getId(), deck.getName(), deck.getCards().stream().map(MysqlModelMapper::toDbModel).collect(Collectors.toSet()), new UserDbModel(deck.getCreatedByUser().getId(), deck.getName()));
    }

    public static CardDbModel toDbModel(Card card) {
        CardDbModel dbModel = new CardDbModel(card.getId(), card.getDeckId(),
                card.getFrontFields().stream().map(MysqlModelMapper::toDbModel).collect(Collectors.toSet()),
                card.getBackFields().stream().map(MysqlModelMapper::toDbModel).collect(Collectors.toSet()),
                toDbModel(card.getScheduleData()));

        return dbModel;
    }

    private static CardScheduleDataDbModel toDbModel(CardScheduleData scheduleData) {
        return new CardScheduleDataDbModel(scheduleData.getId(), scheduleData.getCardId(), scheduleData.getDueDate());
    }

    public static CardFieldDbModel toDbModel(CardField cardField) {
        return new CardFieldDbModel(cardField.getId(), cardField.getCardId(), cardField.getName(), cardField.getText());
    }

    public static Deck toDomainModel(DeckDbModel dbModel) {
        logger.info(dbModel.toString());
        return new Deck(
                dbModel.id,
                dbModel.name,
                toDomainModel(dbModel.user),
                dbModel.cards.stream().map(MysqlModelMapper::toDomainModel).collect(Collectors.toList())
        );
    }

    public static Card toDomainModel(CardDbModel dbModel) {
        return new Card(dbModel.id, dbModel.deckId, toCardFieldList(dbModel.frontFields), toCardFieldList(dbModel.backFields));
    }

    public static User toDomainModel(UserDbModel dbModel) {
        return new User(dbModel.id, dbModel.username, new UserSecurity());
    }

    private static List<CardField> toCardFieldList(Set<CardFieldDbModel> fields) {
        return fields.stream().map(f -> new CardField(f.id, f.cardId, f.name, f.text)).collect(Collectors.toList());
    }
}
