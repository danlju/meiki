package com.danlju.meiki.web.controllers.deck;

import com.danlju.meiki.application.usecases.findalldecks.FindAllDecksUseCase;
import com.danlju.meiki.web.config.AppConfig;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.danlju.meiki.web.controllers.Endpoints.DECKS_URI;
import static com.danlju.meiki.web.controllers.deck.DeckViewModelHelper.toViewModel;

@RestController
public class ListDecksController {

    private final FindAllDecksUseCase findAllDecksUseCase;

    public ListDecksController(FindAllDecksUseCase findAllDecksUseCase) {
        this.findAllDecksUseCase = findAllDecksUseCase;
    }

    @GetMapping(DECKS_URI)
    public ListDecksViewModel list() {
        return toViewModel(
                findAllDecksUseCase.findAll(AppConfig.testUser.getId())
        );
    }
}
