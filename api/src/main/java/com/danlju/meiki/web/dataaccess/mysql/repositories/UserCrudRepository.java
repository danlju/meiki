package com.danlju.meiki.web.dataaccess.mysql.repositories;

import com.danlju.meiki.web.dataaccess.mysql.models.UserDbModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserCrudRepository extends CrudRepository<UserDbModel, String> {
}
