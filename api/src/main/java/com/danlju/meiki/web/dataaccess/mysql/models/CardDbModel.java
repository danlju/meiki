package com.danlju.meiki.web.dataaccess.mysql.models;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = CardDbModel.TABLE_NAME)
public class CardDbModel {

    public static final String TABLE_NAME = "card";

    @Id
    public String id;

    public String deckId;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name="card_front_fields")
    public Set<CardFieldDbModel> frontFields;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name="card_back_fields")
    public Set<CardFieldDbModel> backFields;


    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "card_id", referencedColumnName = "id")
    public CardScheduleDataDbModel scheduleData;

    public CardDbModel() {
    }

    public CardDbModel(String id, String deckId, Set<CardFieldDbModel> frontFields, Set<CardFieldDbModel> backFields, CardScheduleDataDbModel scheduleData) {
        this.id = id;
        this.deckId = deckId;
        this.frontFields = frontFields;
        this.backFields = backFields;
        this.scheduleData = scheduleData;
    }

    @Override
    public String toString() {
        return "CardDbModel{" +
                "id='" + id + '\'' +
                ", deckId='" + deckId + '\'' +
                ", frontFields=" + frontFields +
                ", backFields=" + backFields +
                ", scheduleData=" + scheduleData +
                '}';
    }
}
