package com.danlju.meiki.web.controllers.review;

public class ReviewCardJson {

    public String id;
    public String front;
    public String back;
    public int correctAnswers = 0;
    public int wrongAnswers = 0;

    public ReviewCardJson(String id, String front, String back) {
        this.id = id;
        this.front = front;
        this.back = back;
    }
}
