package com.danlju.meiki.web.controllers.cards;

public class CreateOrUpdateRequest {

    public String id;
    public String deckId;
    public String front;
    public String back;

    @Override
    public String toString() {
        return "CreateCardForm{" +
                "id='" + id + '\'' +
                ", deckId='" + deckId + '\'' +
                ", front='" + front + '\'' +
                ", back='" + back + '\'' +
                '}';
    }
}
