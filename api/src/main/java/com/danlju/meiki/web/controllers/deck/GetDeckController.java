package com.danlju.meiki.web.controllers.deck;

import com.danlju.meiki.application.usecases.models.CardFieldResultModel;
import com.danlju.meiki.application.usecases.models.CardResultModel;
import com.danlju.meiki.application.usecases.getdeck.ShowDeckUseCase;
import com.danlju.meiki.web.controllers.CardViewModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

import static com.danlju.meiki.web.controllers.Endpoints.DECKS_URI;

@RestController
public class GetDeckController {

    private final ShowDeckUseCase showDeckUseCase;

    public GetDeckController(ShowDeckUseCase showDeckUseCase) {
        this.showDeckUseCase = showDeckUseCase;
    }

    @GetMapping(DECKS_URI + "/{id}")
    public ListDecksViewModel.DeckViewModel findById(@PathVariable final String id) {
        var result = showDeckUseCase.findDeckById(id);
        return new ListDecksViewModel.DeckViewModel(result.getId(), result.getName(), result.getCards().size());
    }

    private List<CardViewModel> cardsToJson(List<CardResultModel> cards) {
        return cards.stream().map(
                c -> new CardViewModel(c.getId(), c.getDeckId(), toViewModelList(c.getFrontFields()), toViewModelList(c.getBackFields()), ""))
                .collect(Collectors.toList());
    }

    private List<CardViewModel.CardFieldViewModel> toViewModelList(List<CardFieldResultModel> fields) {
        return fields.stream().map(f -> new CardViewModel.CardFieldViewModel(f.getId(), f.getName(), f.getText())).collect(Collectors.toList());
    }
}
