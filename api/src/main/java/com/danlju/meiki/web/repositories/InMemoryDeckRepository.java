package com.danlju.meiki.web.repositories;

import com.danlju.meiki.core.domain.Deck;
import com.danlju.meiki.core.domain.User;
import com.danlju.meiki.core.repositories.DeckRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Component
public class InMemoryDeckRepository implements DeckRepository {

    private Map<String, Deck> deckMap = new ConcurrentHashMap<>();

    @Override
    public List<Deck> findAll(final User user) {
        return deckMap .values().stream()
                .filter(d -> d.getCreatedByUser().getId().equals(user.getId()))
                .collect(Collectors.toList());
    }

    @Override
    public Deck findById(final String id) {
        return deckMap.get(id);
    }

    @Override
    public Deck create(Deck deck) {
        return deckMap.put(deck.getId(), deck);
    }
}
