package com.danlju.meiki.web.controllers.deck;

import com.danlju.meiki.core.domain.IdUtils;
import com.danlju.meiki.application.usecases.createdeck.CreateDeckUseCase;
import com.danlju.meiki.application.usecases.createdeck.CreateDeckModel;
import com.danlju.meiki.web.config.AppConfig;
import com.danlju.meiki.web.controllers.CreateDeckRequestModel;
import com.danlju.meiki.web.controllers.Endpoints;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CreateDeckController {

    private static final Logger logger = LoggerFactory.getLogger(CreateDeckController.class);

    private final CreateDeckUseCase createDeckUseCase;

    public CreateDeckController(CreateDeckUseCase createDeckUseCase) {
        this.createDeckUseCase = createDeckUseCase;
    }

    @PostMapping(value = Endpoints.DECKS_URI, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ListDecksViewModel.DeckViewModel create(@RequestBody CreateDeckRequestModel requestModel) {

        var id = requestModel.id == null || requestModel.id.isBlank() ? IdUtils.uuid() : requestModel.id;
        var name = requestModel.name;

        var result = createDeckUseCase.create(new CreateDeckModel(id, requestModel.name, AppConfig.testUser.getId()));

        return new ListDecksViewModel.DeckViewModel(id, name, 0);
    }
}
