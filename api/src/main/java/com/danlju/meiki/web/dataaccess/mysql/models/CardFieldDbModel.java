package com.danlju.meiki.web.dataaccess.mysql.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = CardFieldDbModel.TABLE_NAME)
public class CardFieldDbModel {

    public static final String TABLE_NAME = "card_field";

    @Id
    public String id;

    public String cardId;

    public String name;

    public String text;

    public CardFieldDbModel() {
    }

    public CardFieldDbModel(String id, String cardId, String name, String text) {
        this.id = id;
        this.cardId = cardId;
        this.name = name;
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "CardFieldDbModel{" +
                "id='" + id + '\'' +
                ", cardId='" + cardId + '\'' +
                ", name='" + name + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
