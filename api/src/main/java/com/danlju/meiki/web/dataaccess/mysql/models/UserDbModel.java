package com.danlju.meiki.web.dataaccess.mysql.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = UserDbModel.TABLE_NAME)
public class UserDbModel {

    public static final String TABLE_NAME = "user";

    @Id
    public String id;

    public String username;

    public UserDbModel() {
    }

    public UserDbModel(String id, String username) {
        this.id = id;
        this.username = username;
    }

    @Override
    public String toString() {
        return "UserDbModel{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                '}';
    }
}
