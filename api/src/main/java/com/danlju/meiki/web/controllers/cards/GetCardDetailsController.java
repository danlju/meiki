package com.danlju.meiki.web.controllers.cards;

import com.danlju.meiki.application.usecases.getcard.GetCardUseCase;
import com.danlju.meiki.application.usecases.models.CardResultModel;
import com.danlju.meiki.web.controllers.CardViewModel;
import com.danlju.meiki.web.controllers.Endpoints;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;

@RestController
public class GetCardDetailsController {

    private final GetCardUseCase getCardUseCase;

    public GetCardDetailsController(GetCardUseCase getCardUseCase) {
        this.getCardUseCase = getCardUseCase;
    }

    @GetMapping(Endpoints.CARDS_URI + "/{id}")
    public CardViewModel getCard(@PathVariable String id) {
        return toViewModel(getCardUseCase.getCardById(id));
    }

    private CardViewModel toViewModel(CardResultModel resultModel) {
        return new CardViewModel(resultModel.getId(), resultModel.getDeckId(), Collections.emptyList(), Collections.emptyList(), ""); // TODO
    }
}
