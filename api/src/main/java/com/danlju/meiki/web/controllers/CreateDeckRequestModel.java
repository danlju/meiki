package com.danlju.meiki.web.controllers;

public class CreateDeckRequestModel {

    public String id = "";
    public String name;

    public CreateDeckRequestModel(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "CreateDeckForm{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
