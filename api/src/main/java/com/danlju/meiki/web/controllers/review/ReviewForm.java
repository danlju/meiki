package com.danlju.meiki.web.controllers.review;

public class ReviewForm {

    public String cardId;
    public int grade;

    public boolean valid() {
        return cardId != null && !cardId.isBlank();
    }
}
