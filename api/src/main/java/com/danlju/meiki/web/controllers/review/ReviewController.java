package com.danlju.meiki.web.controllers.review;

import com.danlju.meiki.application.usecases.getreviewqueue.GetReviewQueueUseCase;
import com.danlju.meiki.application.usecases.reviewcard.ReviewCardUseCase;
import com.danlju.meiki.application.usecases.reviewcard.ReviewRequestModel;
import com.danlju.meiki.web.controllers.Endpoints;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ReviewController {

    private static final Logger log = LoggerFactory.getLogger(ReviewController.class);

    public static final String baseUri = "/api/reviews";

    private final ReviewCardUseCase reviewCardUseCase;
    private final GetReviewQueueUseCase reviewQueueUseCase;

    public ReviewController(ReviewCardUseCase reviewCardUseCase, GetReviewQueueUseCase reviewQueueUseCase) {
        this.reviewCardUseCase = reviewCardUseCase;
        this.reviewQueueUseCase = reviewQueueUseCase;
    }

    @PostMapping(baseUri)
    public String reviewCard(@RequestBody ReviewForm form) {

        if (!form.valid()) {
            // TODO : Handle
            log.info("ReviewForm invalid");

            return "error";
        }

        reviewCardUseCase.doReview(toReviewModel(form));

        return "success"; // TODO
    }

    private ReviewRequestModel toReviewModel(ReviewForm form) {
        return new ReviewRequestModel(form.cardId, form.grade);
    }

    @GetMapping(Endpoints.CARDS_URI + "/due/{deckId}") // TODO:3
    public List<ReviewCardJson> getDueCards(@PathVariable final String deckId) {

        var result = reviewQueueUseCase.getReviewQueue(deckId);

        List<ReviewCardJson> reviewCards = new ArrayList<>();

        return result.cards.stream().map(c ->
            new ReviewCardJson(c.id, c.front, c.back)
        ).collect(Collectors.toList());
    }
}
