package com.danlju.meiki.application.usecases.reviewcard;

public interface ReviewCardUseCase {
    void doReview(ReviewRequestModel model);
}
