package com.danlju.meiki.application.usecases.getreviewqueue;

public interface GetReviewQueueUseCase {
    GetReviewQueueResult getReviewQueue(String deckId);
}
