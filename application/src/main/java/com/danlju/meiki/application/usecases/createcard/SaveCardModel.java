package com.danlju.meiki.application.usecases.createcard;

public class SaveCardModel {

    private String id;
    private String deckId;
    private String front;
    private String back;

    public SaveCardModel(String id, String deckId, String front, String back) {
        this.id = id;
        this.deckId = deckId;
        this.front = front;
        this.back = back;
    }

    public String getId() {
        return id;
    }

    public String getDeckId() {
        return deckId;
    }

    public String getFront() {
        return front;
    }

    public String getBack() {
        return back;
    }

    @Override
    public String toString() {
        return "CardCreationModel{" +
                "id='" + id + '\'' +
                ", deckId='" + deckId + '\'' +
                ", front='" + front + '\'' +
                ", back='" + back + '\'' +
                '}';
    }
}
