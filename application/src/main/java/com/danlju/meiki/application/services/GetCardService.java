package com.danlju.meiki.application.services;

import com.danlju.meiki.core.domain.Card;
import com.danlju.meiki.core.repositories.CardRepository;
import com.danlju.meiki.application.usecases.models.CardResultModel;
import com.danlju.meiki.application.usecases.getcard.GetCardUseCase;

public class GetCardService implements GetCardUseCase {

    private final CardRepository cardRepository;

    public GetCardService(CardRepository cardRepository) {
        this.cardRepository = cardRepository;
    }

    @Override
    public CardResultModel getCardById(final String id) {
        return toResultModel(
                cardRepository.findById(id)
        );
    }

    private CardResultModel toResultModel(final Card card) {
        return new CardResultModel(card.getId(), card.getDeckId(),
                CardResultModel.toResultModelFields(card.getFrontFields()),
                CardResultModel.toResultModelFields(card.getBackFields()));
    }
}
