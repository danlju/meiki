package com.danlju.meiki.application.services;

import com.danlju.meiki.core.domain.Card;
import com.danlju.meiki.core.domain.CardScheduleData;
import com.danlju.meiki.core.domain.IdUtils;
import com.danlju.meiki.core.repositories.CardRepository;
import com.danlju.meiki.application.usecases.createcard.SaveCardModel;
import com.danlju.meiki.application.usecases.createcard.SaveOrUpdateCardUseCase;

import java.time.Instant;

public class SaveOrUpdateCardService implements SaveOrUpdateCardUseCase {

    private final CardRepository cardRepository;

    public SaveOrUpdateCardService(CardRepository cardRepository) {
        this.cardRepository = cardRepository;
    }

    @Override
    public void saveOrUpdate(SaveCardModel model) {
        cardRepository.saveOrUpdate(
                toCard(model)
        );
    }

    private Card toCard(SaveCardModel model) {

        var id = model.getId() == null ? IdUtils.uuid() : model.getId();

        return new Card(id, model.getDeckId(), model.getFront(), model.getBack(),
                new CardScheduleData(IdUtils.uuid(), id, Instant.now()));
    }
}
