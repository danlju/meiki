package com.danlju.meiki.application.usecases.reviewcard;

public class ReviewRequestModel {
    public String cardId;
    public int userGrade;

    public ReviewRequestModel(String cardId, int userGrade) {
        this.cardId = cardId;
        this.userGrade = userGrade;
    }
}
