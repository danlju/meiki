package com.danlju.meiki.application.usecases.createdeck;

import com.danlju.meiki.application.services.CreateDeckResult;

public interface CreateDeckUseCase {
    CreateDeckResult create(CreateDeckModel model);
}
