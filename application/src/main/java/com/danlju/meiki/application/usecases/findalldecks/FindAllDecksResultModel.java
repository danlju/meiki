package com.danlju.meiki.application.usecases.findalldecks;

import com.danlju.meiki.application.usecases.models.DeckResultModel;

import java.util.Collections;
import java.util.List;

public class FindAllDecksResultModel {

    private List<DeckResultModel> decks;

    public FindAllDecksResultModel(List<DeckResultModel> decks) {
        this.decks = decks;
    }

    public List<DeckResultModel> getDecks() {
        return decks;
    }

    public static final FindAllDecksResultModel EMPTY = new FindAllDecksResultModel(Collections.emptyList());// TODO
}
