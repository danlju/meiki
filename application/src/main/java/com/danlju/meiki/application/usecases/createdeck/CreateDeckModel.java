package com.danlju.meiki.application.usecases.createdeck;

public class CreateDeckModel {

    private final String id;
    private final String name;
    private final String userId;

    public CreateDeckModel(String id, String name, String userId) {
        this.id = id;
        this.name = name;
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUserId() {
        return userId;
    }
}
