package com.danlju.meiki.application.usecases.models;

import com.danlju.meiki.core.domain.CardField;

import java.util.List;
import java.util.stream.Collectors;

public class CardResultModel {

    private String id;
    private String deckId;
    private List<CardFieldResultModel> frontFields;
    private List<CardFieldResultModel> backFields;

    public CardResultModel(String id, String deckId, List<CardFieldResultModel> frontFields, List<CardFieldResultModel> backFields) {
        this.id = id;
        this.frontFields = frontFields;
        this.backFields = backFields;
    }

    public String getId() {
        return id;
    }

    public String getDeckId() {
        return deckId;
    }

    public List<CardFieldResultModel> getFrontFields() {
        return frontFields;
    }

    public List<CardFieldResultModel> getBackFields() {
        return backFields;
    }

    public static List<CardFieldResultModel> toResultModelFields(List<CardField> fields) {
        return fields.stream().map(f -> new CardFieldResultModel(f.getId(), f.getName(), f.getText()))
                .collect(Collectors.toList());
    }
}