package com.danlju.meiki.application.usecases.getcard;

import com.danlju.meiki.application.usecases.models.CardResultModel;

public interface GetCardUseCase {
    CardResultModel getCardById(final String id);
}
