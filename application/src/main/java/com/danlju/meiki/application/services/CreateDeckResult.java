package com.danlju.meiki.application.services;

public class CreateDeckResult {

    public String id;
    public String errorMessage;

    public CreateDeckResult(String id) {
        this.id = id;
    }

    public CreateDeckResult(String id, String errorMessage) {
        this.id = id;
        this.errorMessage = errorMessage;
    }

    public static CreateDeckResult error(String errorMessage) {

       CreateDeckResult errorResult = new CreateDeckResult("", errorMessage);
       errorResult.errorMessage = errorMessage;

       return errorResult;
    }

    public boolean hasError() {
        return errorMessage != null && !errorMessage.isBlank();
    }
}
