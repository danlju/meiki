package com.danlju.meiki.application.usecases.createcard;

public interface SaveOrUpdateCardUseCase {
    void saveOrUpdate(SaveCardModel model);
}
