package com.danlju.meiki.application.usecases.getdeck;

import com.danlju.meiki.application.usecases.models.DeckResultModel;

public interface ShowDeckUseCase {
    DeckResultModel findDeckById(final String deckId);
}
