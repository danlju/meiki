package com.danlju.meiki.application.usecases.findalldecks;

public interface FindAllDecksUseCase {

    FindAllDecksResultModel findAll(final String userId);
}
