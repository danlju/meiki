package com.danlju.meiki.application.services;

import com.danlju.meiki.application.usecases.getreviewqueue.GetReviewQueueResult;
import com.danlju.meiki.application.usecases.getreviewqueue.GetReviewQueueUseCase;
import com.danlju.meiki.core.domain.Card;
import com.danlju.meiki.core.repositories.CardRepository;

import java.util.List;

public class GetReviewQueueService implements GetReviewQueueUseCase {

    private final CardRepository cardRepository;

    public GetReviewQueueService(CardRepository cardRepository) {
        this.cardRepository = cardRepository;
    }

    @Override
    public GetReviewQueueResult getReviewQueue(final String deckId) {

        List<Card> cards = cardRepository.findDueCards(deckId);

        GetReviewQueueResult result = new GetReviewQueueResult();
        result.deckId = deckId;

        return toResultModel(deckId, cards);
    }

    private GetReviewQueueResult toResultModel(String deckId, List<Card> cards) {

        GetReviewQueueResult result = new GetReviewQueueResult();
        result.deckId = deckId;

        if (cards == null) {
            return result;
        }

        // TODO: fix
     //   cards.forEach(
     //           c -> result.cards.add(
       //                new GetReviewQueueResult.ReviewCard(c.getId(), );
       //        )
   //     );

        return result;
    }
}