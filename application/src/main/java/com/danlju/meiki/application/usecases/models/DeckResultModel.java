package com.danlju.meiki.application.usecases.models;

import java.util.ArrayList;
import java.util.List;

public class DeckResultModel {

    public static final DeckResultModel EMPTY = new DeckResultModel("", "", new ArrayList<>());

    private String id;
    private String name;
    private List<CardResultModel> cards;

    public DeckResultModel(String id, String name, List<CardResultModel> cards) {
        this.id = id;
        this.name = name;
        this.cards = cards;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<CardResultModel> getCards() {
        return cards;
    }
}
