package com.danlju.meiki.application.usecases.models;

public class CardFieldResultModel {

    private String id;

    private String name;

    private String text;

    public CardFieldResultModel(String id, String cardId, String name, String text) {
        this.id = id;
        this.text = text;
    }

    public CardFieldResultModel(String id, String name, String text) {
        this.id = id;
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getText() {
        return text;
    }
}
