package com.danlju.meiki.application.services;

import com.danlju.meiki.application.usecases.reviewcard.ReviewCardUseCase;
import com.danlju.meiki.application.usecases.reviewcard.ReviewRequestModel;
import com.danlju.meiki.core.domain.Card;
import com.danlju.meiki.core.domain.CardReview;
import com.danlju.meiki.core.domain.IdUtils;
import com.danlju.meiki.core.domain.ReviewData;
import com.danlju.meiki.core.repositories.CardRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;

public class ReviewService implements ReviewCardUseCase {

    private static final Logger logger = LoggerFactory.getLogger(ReviewService.class);

    //private CardReviewRepository reviewRepository;
    private CardRepository cardRepository;

    public ReviewService(CardRepository cardRepository) {
        this.cardRepository = cardRepository;
    }

    @Override
    public void doReview(ReviewRequestModel model) {

        Card card = cardRepository.findById(model.cardId);

        if (card == null || "".equals(card.getId())) {
            throw new RuntimeException("Card not found"); // TODO
        }

        logger.info("Before {}", card.getReviewData().toString());

        updateReviewData(card.getReviewData(), model);

        rescheduleCard(card);

        cardRepository.saveOrUpdate(card);
        logger.info("After {}", card.getReviewData().toString());

    }

    private void rescheduleCard(Card card) {

        Instant currentDueDate = card.getScheduleData().getDueDate();
        long seconds = (long) card.getReviewData().getInterval() * 3600 * 24;
        logger.info("Adding {}", seconds);
        card.getScheduleData().setDueDate(currentDueDate.plusSeconds(seconds));
    }

    /**
     * Modifies a ReviewData object with new values based on how the user graded the card
     * using the SuperMemo2 algorithm
     */
    private void updateReviewData(ReviewData reviewData, ReviewRequestModel model) {

        if (model.userGrade >= 3) {

            if (reviewData.getRepetitionNumber() == 0) {
                reviewData.setInterval(1);
            } else if (reviewData.getRepetitionNumber() == 1) {
                reviewData.setInterval(6);
            } else {
                reviewData.increaseIntervalByEaseFactor();
            }
            reviewData.increaseRepetitionNumber();
        } else {

            reviewData.setRepetitionNumber(0);
            reviewData.setInterval(1);
        }


        float newEaseFactor = reviewData.getEaseFactor() + (0.1f - (5 - model.userGrade) * (0.08f + (5 - model.userGrade) * 0.02f));

        reviewData.setEaseFactor(Math.max(newEaseFactor, 1.3f));
    }

    private CardReview toReviewObject(ReviewRequestModel model) {
        return new CardReview(IdUtils.uuid(), model.cardId, Instant.now(), model.userGrade);
    }

}
