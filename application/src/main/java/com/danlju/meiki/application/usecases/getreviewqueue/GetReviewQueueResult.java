package com.danlju.meiki.application.usecases.getreviewqueue;

import java.util.ArrayList;
import java.util.List;

public class GetReviewQueueResult {

    public String deckId;
    public List<ReviewCard> cards = new ArrayList<>();

    public static class ReviewCard {

        public String id;
        public String front;
        public String back;

        public ReviewCard(String id, String front, String back) {
            this.id = id;
            this.front = front;
            this.back = back;
        }
    }

}
