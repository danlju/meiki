package com.danlju.meiki.application.services;

import com.danlju.meiki.application.usecases.models.CardFieldResultModel;
import com.danlju.meiki.core.domain.Card;
import com.danlju.meiki.core.domain.CardField;
import com.danlju.meiki.core.domain.Deck;
import com.danlju.meiki.core.repositories.DeckRepository;
import com.danlju.meiki.core.repositories.UserRepository;
import com.danlju.meiki.application.usecases.models.CardResultModel;
import com.danlju.meiki.application.usecases.models.DeckResultModel;
import com.danlju.meiki.application.usecases.findalldecks.FindAllDecksResultModel;
import com.danlju.meiki.application.usecases.findalldecks.FindAllDecksUseCase;

import java.util.List;
import java.util.stream.Collectors;

public class FindAllDecksService implements FindAllDecksUseCase {

    private DeckRepository deckRepository;
    private UserRepository userRepository;

    public FindAllDecksService(DeckRepository deckRepository,
                               UserRepository userRepository) {
        this.deckRepository = deckRepository;
        this.userRepository = userRepository;
    }

    @Override
    public FindAllDecksResultModel findAll(final String userId) {
        var user = userRepository.findById(userId);
        return toResultModel(
                deckRepository.findAll(user)
        );
    }

    private FindAllDecksResultModel toResultModel(List<Deck> decks) {

        if (decks == null || decks.isEmpty()) {
            return FindAllDecksResultModel.EMPTY;
        }

        List<DeckResultModel> deckResults = decks.stream().map(
                deck -> new DeckResultModel(deck.getId(), deck.getName(), toCardResults(deck.getCards()))
        ).collect(Collectors.toList());

        return new FindAllDecksResultModel(deckResults);
    }

    private List<CardResultModel> toCardResults(List<Card> cards) {
        return cards.stream().map(
                c -> new CardResultModel(c.getId(), c.getDeckId(), toResultModelFields(c.getFrontFields()), toResultModelFields(c.getBackFields()))
        ).collect(Collectors.toList());
    }

    private List<CardFieldResultModel> toResultModelFields(List<CardField> fields) {
        return fields.stream().map(f -> new CardFieldResultModel(f.getId(), f.getName(), f.getText()))
                .collect(Collectors.toList());
    }
}
