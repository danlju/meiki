package com.danlju.meiki.application.services;

import com.danlju.meiki.core.domain.Deck;
import com.danlju.meiki.core.repositories.DeckRepository;
import com.danlju.meiki.core.repositories.UserRepository;
import com.danlju.meiki.application.usecases.createdeck.CreateDeckUseCase;
import com.danlju.meiki.application.usecases.createdeck.CreateDeckModel;

import java.util.ArrayList;

public class CreateDeckService implements CreateDeckUseCase {

    private final DeckRepository deckRepository;
    private final UserRepository userRepository;

    public CreateDeckService(DeckRepository deckRepository, UserRepository userRepository) {
        this.deckRepository = deckRepository;
        this.userRepository = userRepository;
    }

    @Override
    public CreateDeckResult create(CreateDeckModel model) {

        if (model.getName() == null || model.getName().isBlank()) {
            return CreateDeckResult.error("Missing fields");
        }

        deckRepository.create(
                toDeck(model)
        );

        return new CreateDeckResult(model.getId());
    }

    private Deck toDeck(CreateDeckModel model) {
        var user = userRepository.findById(model.getUserId());

        return new Deck(model.getId(), model.getName(), user, new ArrayList<>());
    }
}
