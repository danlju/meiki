package com.danlju.meiki.application.services;

import com.danlju.meiki.application.usecases.models.CardFieldResultModel;
import com.danlju.meiki.core.domain.Card;
import com.danlju.meiki.core.domain.CardField;
import com.danlju.meiki.core.domain.Deck;
import com.danlju.meiki.core.repositories.DeckRepository;
import com.danlju.meiki.application.usecases.models.CardResultModel;
import com.danlju.meiki.application.usecases.models.DeckResultModel;
import com.danlju.meiki.application.usecases.getdeck.ShowDeckUseCase;

import java.util.List;
import java.util.stream.Collectors;

public class ShowDeckService implements ShowDeckUseCase {

    private final DeckRepository deckRepository;

    public ShowDeckService(DeckRepository deckRepository) {
        this.deckRepository = deckRepository;
    }

    @Override
    public DeckResultModel findDeckById(String id) {

        var deck = deckRepository.findById(id);

        if (deck == null || deck.getId() == null || deck.getId().equals("")) {
            return DeckResultModel.EMPTY;
        }

        return toResultModel(deck);
    }

    // TODO: maybe move to a common class
    private DeckResultModel toResultModel(Deck deck) {
        return new DeckResultModel(deck.getId(), deck.getName(), toCardResults(deck.getCards()));
    }

    private List<CardResultModel> toCardResults(List<Card> cards) {
        return cards.stream().map(
                c -> new CardResultModel(c.getId(), c.getDeckId(), CardResultModel.toResultModelFields(c.getFrontFields()), CardResultModel.toResultModelFields(c.getBackFields()))
        ).collect(Collectors.toList());
    }

}
