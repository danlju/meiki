package com.danlju.meiki.application.services;

import com.danlju.meiki.core.domain.*;
import com.danlju.meiki.application.usecases.findalldecks.FindAllDecksResultModel;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class FindAllDecksServiceTest {

    DeckRepositoryMock deckRepository;
    UserRepositoryMock userRepository;
    FindAllDecksService findAllDecksService;


    @Test
    public void should_call_repository_method() {

        deckRepository = new DeckRepositoryMock();
        userRepository = new UserRepositoryMock();

        findAllDecksService = new FindAllDecksService(deckRepository, userRepository);

        findAllDecksService.findAll(IdUtils.uuid());

        assertThat(deckRepository.findAllCalled, is(true));
    }

    @Test
    public void should_convert_domain_to_result() {

        deckRepository = new DeckRepositoryMock();
        userRepository = new UserRepositoryMock();

        findAllDecksService = new FindAllDecksService(deckRepository, userRepository);

        List<Card> cards = new ArrayList<>();
        // TODO
        cards.add(new Card(IdUtils.uuid(), IdUtils.uuid(), "front", "back"));
        cards.add(new Card(IdUtils.uuid(), IdUtils.uuid(), "front 2", "back 2"));

        Deck deck = new Deck(IdUtils.uuid(), "A deck", new User(IdUtils.uuid(), "test-user", new UserSecurity()), cards);

        deckRepository.decks.add(deck);

        FindAllDecksResultModel resultModel = findAllDecksService.findAll(IdUtils.uuid());

        assertThat(resultModel.getDecks().size(), is(1));
        assertThat(resultModel.getDecks().get(0).getCards().size(), is(cards.size()));
    }
}