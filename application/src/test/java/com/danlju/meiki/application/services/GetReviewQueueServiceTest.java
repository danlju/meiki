package com.danlju.meiki.application.services;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class GetReviewQueueServiceTest {

    @Test
    public void should_return_a_valid_result() {

        CardRepositoryMock repositoryMock = new CardRepositoryMock();
        GetReviewQueueService service = new GetReviewQueueService(repositoryMock);

        var result = service.getReviewQueue(repositoryMock.deckId);

        // TODO: enable after fixing
//        assertThat(result.deckId, is(repositoryMock.deckId));
//        assertThat(result.cards.size(), is(repositoryMock.cardList.size()));

    }
}