package com.danlju.meiki.application.services;

import com.danlju.meiki.core.domain.*;

import java.util.ArrayList;
import java.util.List;

public class TestData {

    private static List<Card> cards;
    private static Deck deck;

    static {

        final String deckId = IdUtils.uuid();

        List<Card> cards = new ArrayList<>();

        String cardId = IdUtils.uuid();

        List<CardField> frontFields = List.of(
                new CardField(cardId, IdUtils.uuid(), "front 1", "front 1"),
                new CardField(cardId, IdUtils.uuid(), "front 2", "front 2"));

        List<CardField> backFields = List.of(
                new CardField(cardId, IdUtils.uuid(), "back 1", "back 1"),
                new CardField(cardId, IdUtils.uuid(), "back 2", "back 2"));


        cards.add(new Card(cardId, deckId,  frontFields, backFields));
        cards.add(new Card(IdUtils.uuid(), deckId, "交差点", "crossing, intersection"));


        deck = new Deck(deckId, "My deck", new User(IdUtils.uuid(), "testUser", new UserSecurity()), cards);
    }

    public static Deck deck() {
        return deck;
    }

    public static List<Card> cards() {
        return cards;
    }

}
