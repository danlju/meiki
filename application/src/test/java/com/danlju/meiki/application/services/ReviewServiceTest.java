package com.danlju.meiki.application.services;

import com.danlju.meiki.application.usecases.reviewcard.ReviewRequestModel;
import com.danlju.meiki.core.domain.Card;
import org.junit.jupiter.api.Test;

class ReviewServiceTest {

    @Test
    public void should_update_cards_review_data() {

        CardRepositoryMock cardRepositoryMock = new CardRepositoryMock();

        Card card = cardRepositoryMock.cardList.get(0);

        ReviewService reviewService = new ReviewService(cardRepositoryMock);
        System.out.println(card.getReviewData() + "\n\n");
        System.out.println(card.getScheduleData() + "\n\n");

        reviewService.doReview(new ReviewRequestModel(card.getId(), 5));

        System.out.println(card.getReviewData() + "\n\n");
        System.out.println(card.getScheduleData() + "\n\n");

        reviewService.doReview(new ReviewRequestModel(card.getId(), 5));

        System.out.println(card.getReviewData() + "\n\n");
        System.out.println(card.getScheduleData() + "\n\n");

    }
}