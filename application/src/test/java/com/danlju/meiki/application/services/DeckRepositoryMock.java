package com.danlju.meiki.application.services;

import com.danlju.meiki.core.domain.Deck;
import com.danlju.meiki.core.domain.User;
import com.danlju.meiki.core.repositories.DeckRepository;

import java.util.ArrayList;
import java.util.List;

public class DeckRepositoryMock implements DeckRepository {

    public boolean findAllCalled = false;
    public List<Deck> decks = new ArrayList<>();

    public Deck createdDeck;
    public String showId;
    public boolean returnNotFound = false;

    @Override
    public List<Deck> findAll(User user) {

        findAllCalled = true;
        return decks;
    }

    @Override
    public Deck findById(final String id) {

        this.showId = id;

        return returnNotFound ? null : TestData.deck();
    }

    @Override
    public Deck create(Deck deck) {
        createdDeck = deck;
        return createdDeck;
    }
}
