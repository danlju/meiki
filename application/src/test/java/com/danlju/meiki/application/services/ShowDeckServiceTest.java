package com.danlju.meiki.application.services;

import com.danlju.meiki.application.usecases.models.DeckResultModel;
import com.danlju.meiki.core.domain.IdUtils;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class ShowDeckServiceTest {

    @Test
    public void should_be_wired() {

        DeckRepositoryMock deckRepositoryMock = new DeckRepositoryMock();

        var showDeckService = new ShowDeckService(deckRepositoryMock);

        final String id = IdUtils.uuid();

        var result = showDeckService.findDeckById(id);

        assertThat(deckRepositoryMock.showId, is(id));
        assertThat(result.getId(), is(TestData.deck().getId()));
        assertThat(result.getName(), is(TestData.deck().getName()));
        assertThat(result.getCards().size(), is(TestData.deck().getCards().size()));
    }

    @Test
    public void should_return_empty_when_deck_is_not_found() {

        DeckRepositoryMock deckRepositoryMock = new DeckRepositoryMock();
        var showDeckService = new ShowDeckService(deckRepositoryMock);
        final String id = IdUtils.uuid();

        deckRepositoryMock.returnNotFound = true;

        var result = showDeckService.findDeckById(id);

        assertThat(result, is(DeckResultModel.EMPTY));
    }

}