package com.danlju.meiki.application.services;

import com.danlju.meiki.core.domain.IdUtils;
import com.danlju.meiki.core.domain.User;
import com.danlju.meiki.core.domain.UserSecurity;
import com.danlju.meiki.application.usecases.createdeck.CreateDeckModel;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class CreateDeckServiceTest {

    @Test
    public void should_call_repository_save() {

        DeckRepositoryMock deckRepositoryMock = new DeckRepositoryMock();
        UserRepositoryMock userRepositoryMock = new UserRepositoryMock();
        CreateDeckService createDeckService = new CreateDeckService(deckRepositoryMock, userRepositoryMock);

        var user = new User(IdUtils.uuid(), "testUser", new UserSecurity());
        userRepositoryMock.userMock = user;

        var creationModel = new CreateDeckModel(IdUtils.uuid(), "New deck", user.getId());

        createDeckService.create(creationModel);

        assertThat(deckRepositoryMock.createdDeck.getId(), is(creationModel.getId()));
        assertThat(deckRepositoryMock.createdDeck.getName(), is(creationModel.getName()));
        assertThat(deckRepositoryMock.createdDeck.getCreatedByUser().getId(), is(creationModel.getUserId()));
    }

    @Test
    public void should_handle_missing_fields() {

        DeckRepositoryMock deckRepositoryMock = new DeckRepositoryMock();
        UserRepositoryMock userRepositoryMock = new UserRepositoryMock();
        CreateDeckService createDeckService = new CreateDeckService(deckRepositoryMock, userRepositoryMock);

        var user = new User(IdUtils.uuid(), "testUser", new UserSecurity());
        userRepositoryMock.userMock = user;

        var incompleteModel = new CreateDeckModel(IdUtils.uuid(), "", user.getId());

        var result = createDeckService.create(incompleteModel);

        assertThat(result.hasError(), is(true));
        assertThat(result.errorMessage.length(), is(greaterThan(0)));
    }
}
