package com.danlju.meiki.application.services;

import com.danlju.meiki.core.domain.Card;
import com.danlju.meiki.core.domain.IdUtils;
import com.danlju.meiki.core.repositories.CardRepository;

import java.util.ArrayList;
import java.util.List;

public class CardRepositoryMock implements CardRepository {

    public Card savedCard;
    public List<Card> cardList = new ArrayList<>();
    public String deckId = IdUtils.uuid();

    public CardRepositoryMock() {
        cardList.add(new Card(IdUtils.uuid(), deckId, "Front 1", "Front 2"));
        cardList.add(new Card(IdUtils.uuid(), deckId, "Front 1", "Front 2"));
        cardList.add(new Card(IdUtils.uuid(), deckId, "Front 1", "Front 2"));
        cardList.add(new Card(IdUtils.uuid(), deckId, "Front 1", "Front 2"));
    }

    @Override
    public Card findById(String id) {
        return cardList.stream().filter(card -> card.getId().equals(id)).findFirst().get();
    }

    @Override
    public List<Card> findByDeckId(String deckId) {
        return null;
    }

    @Override
    public Card saveOrUpdate(Card card) {
        savedCard = card;
        return card;
    }

    @Override
    public List<Card> findDueCards(String deckId) {
        return cardList;
    }
}
