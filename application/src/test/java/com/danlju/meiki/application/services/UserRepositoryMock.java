package com.danlju.meiki.application.services;

import com.danlju.meiki.core.domain.User;
import com.danlju.meiki.core.repositories.UserRepository;

public class UserRepositoryMock implements UserRepository {

    public User userMock;

    @Override
    public User findById(String id) {
        return userMock;
    }

    @Override
    public void save(User user) {

    }
}
