package com.danlju.meiki.application.services;

import com.danlju.meiki.core.domain.IdUtils;
import com.danlju.meiki.application.usecases.createcard.SaveCardModel;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class CreateCardServiceTest {

    @Test
    public void verify_wiring() {

        var repository = new CardRepositoryMock();
        var service = new SaveOrUpdateCardService(repository);

        var model = new SaveCardModel(IdUtils.uuid(), IdUtils.uuid(), "Front", "Back");
        service.saveOrUpdate(model);

        assertNotNull(repository.savedCard);
    }

    @Test
    public void verify_copy_properties() {

        var repository = new CardRepositoryMock();
        var service = new SaveOrUpdateCardService(repository);

        var model = new SaveCardModel(IdUtils.uuid(), IdUtils.uuid(), "Front", "Back");
        service.saveOrUpdate(model);

        assertThat(repository.savedCard.getId(), is(model.getId()));
        assertThat(repository.savedCard.getDeckId(), is(model.getDeckId()));
        assertThat(repository.savedCard.getFrontFields().get(0).getText(), is(model.getFront()));
        assertThat(repository.savedCard.getBackFields().get(0).getText(), is(model.getBack()));
        
    }
}