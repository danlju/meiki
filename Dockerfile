FROM adoptopenjdk/openjdk15:alpine-jre

WORKDIR /opt/app
ARG JAR_FILE=api/target/api-0.0.1.jar

RUN cd /opt/app

# cp target/spring-boot-web.jar /opt/app/app.jar
COPY ${JAR_FILE} app.jar

# java -jar /opt/app/app.jar
ENTRYPOINT ["java","-jar","app.jar"]