package com.danlju.meiki.core.domain;

import java.time.Instant;

public class CardReview {

    private String id;
    private String cardId;
    private Instant date;
    private int grade;

    public CardReview(String id, String cardId, Instant date, int grade) {
        this.id = id;
        this.cardId = cardId;
        this.date = date;
        this.grade = grade;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public int getGrade() {
        return grade;
    }
}
