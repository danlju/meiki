package com.danlju.meiki.core.domain;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class Card {

    private final String id;
    private final String deckId;
    private List<CardField> frontFields;
    private List<CardField> backFields;
    private ReviewData reviewData;
    private CardScheduleData scheduleData;
    private List<CardReview> reviews;


    public Card(String id, String deckId, List<CardField> frontFields, List<CardField> backFields, ReviewData reviewData, CardScheduleData scheduleData, List<CardReview> reviews) {
        this.id = id;
        this.deckId = deckId;
        this.frontFields = frontFields;
        this.backFields = backFields;
        this.reviewData = reviewData;
        this.scheduleData = scheduleData;
        this.reviews = reviews;
    }

    public Card(String id, String deckId, List<CardField> frontFields, List<CardField> backFields) {

        this.id = id;
        this.deckId = deckId;
        this.frontFields = frontFields;
        this.backFields = backFields;

        this.reviewData = new ReviewData(IdUtils.uuid(), id, 2.5f, 1, 0);
        this.reviews = new ArrayList<>();
        this.scheduleData = new CardScheduleData(IdUtils.uuid(), id, Instant.now());

    }



    public Card(String id, String deckId, String front, String back) {

        this.id = id;
        this.deckId = deckId;
        this.frontFields = List.of(new CardField(IdUtils.uuid(), id, "front", front)); // TODO: names
        this.backFields = List.of(new CardField(IdUtils.uuid(), id, "back", back));

        this.reviewData = new ReviewData(IdUtils.uuid(), id, 2.5f, 1,0);
        this.reviews = new ArrayList<>();
        this.scheduleData = new CardScheduleData(IdUtils.uuid(), id, Instant.now());

    }

    public Card(String id, String deckId, String front, String back, CardScheduleData cardScheduleData) {
        this.id = id;
        this.deckId = deckId;
        this.frontFields = List.of(new CardField(IdUtils.uuid(), id, "front", front));
        this.backFields = List.of(new CardField(IdUtils.uuid(), id, "back", back));
        this.scheduleData = cardScheduleData;
    }

    public String getId() {
        return id;
    }

    public String getDeckId() {
        return deckId;
    }

    public List<CardField> getFrontFields() {
        return frontFields;
    }

    public List<CardField> getBackFields() {
        return backFields;
    }

    public ReviewData getReviewData() {
        return reviewData;
    }

    public CardScheduleData getScheduleData() {
        return scheduleData;
    }

    public void setScheduleData(CardScheduleData scheduleData) {
        this.scheduleData = scheduleData;
    }
}
