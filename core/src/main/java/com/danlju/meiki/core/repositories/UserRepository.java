package com.danlju.meiki.core.repositories;

import com.danlju.meiki.core.domain.User;

public interface UserRepository {

    User findById(final String id);

    void save(final User user);
}
