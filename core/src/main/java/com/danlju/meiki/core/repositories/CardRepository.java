package com.danlju.meiki.core.repositories;

import com.danlju.meiki.core.domain.Card;

import java.util.List;

public interface CardRepository {

    Card findById(final String id);

    List<Card> findByDeckId(final String deckId);

    Card saveOrUpdate(final Card card);

    List<Card> findDueCards(String deckId);
}
