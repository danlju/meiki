package com.danlju.meiki.core.domain;

public class CardField {

    private String id;

    private String cardId;

    private String name;

    private String text;

    public CardField(String id, String cardId, String name, String text) {
        this.id = id;
        this.cardId = cardId;
        this.name = name;
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public String getCardId() {
        return cardId;
    }

    public String getName() {
        return name;
    }

    public String getText() {
        return text;
    }
}
