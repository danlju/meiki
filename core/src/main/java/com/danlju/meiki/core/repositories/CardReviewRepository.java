package com.danlju.meiki.core.repositories;

import com.danlju.meiki.core.domain.CardReview;

public interface CardReviewRepository {
    void save(CardReview cardReview);
}
