package com.danlju.meiki.core.domain;

import java.time.Instant;

public class CardScheduleData {

    private final String id;
    private final String cardId;
    private Instant dueDate;

    public CardScheduleData(String id, String cardId, Instant dueDate) {
        this.id = id;
        this.cardId = cardId;
        this.dueDate = dueDate;
    }

    public String getId() {
        return id;
    }

    public String getCardId() {
        return cardId;
    }

    public Instant getDueDate() {
        return dueDate;
    }

    public void setDueDate(Instant dueDate) {
        this.dueDate = dueDate;
    }

    @Override
    public String toString() {
        return "CardScheduleData{" +
                "id='" + id + '\'' +
                ", cardId='" + cardId + '\'' +
                ", dueDate=" + dueDate +
                '}';
    }
}
