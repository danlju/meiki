package com.danlju.meiki.core.domain;

public class User {

    private String id;
    private String username;
    private UserSecurity security;

    public User(String id, String username, UserSecurity security) {
        this.id = id;
        this.username = username;
        this.security = security;
    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public UserSecurity getSecurity() {
        return security;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                '}';
    }
}
