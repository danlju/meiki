package com.danlju.meiki.core.repositories;

import com.danlju.meiki.core.domain.Deck;
import com.danlju.meiki.core.domain.User;

import java.util.List;

public interface DeckRepository {

    List<Deck> findAll(final User user);

    Deck findById(final String id);

    Deck create(final Deck deck);
}
