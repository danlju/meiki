package com.danlju.meiki.core.domain;

import java.util.UUID;

public class IdUtils {

    public static String uuid() {
        return UUID.randomUUID().toString();
    }
}
