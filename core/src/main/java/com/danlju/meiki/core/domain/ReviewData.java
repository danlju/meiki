package com.danlju.meiki.core.domain;

public class ReviewData {

    private String id;

    private String cardId;

    /*
        The easiness factor EF, which loosely indicates how "easy" the card is (more precisely,
        it determines how quickly the inter-repetition interval grows). The initial value of EF is 2.5.
    */
    private float easeFactor;

    /*
        The inter-repetition interval, which is the length of time (in days) we will wait after the previous
        review before asking the user to review the card again.
    */
    private int interval;

    /*
        The repetition number, which is the number of times the card has been successfully recalled
        (meaning it was given a grade ≥ 3) in a row since the last time it was not.
    */
    private int repetitionNumber;


    public ReviewData(String id, String cardId, float easeFactor, int interval, int repetitionNumber) {
        this.id = id;
        this.cardId = cardId;
        this.easeFactor = easeFactor;
        this.interval = interval;
        this.repetitionNumber = repetitionNumber;
    }

    public float getEaseFactor() {
        return easeFactor;
    }

    public void setEaseFactor(float easeFactor) {
        this.easeFactor = easeFactor;
    }

    public String getId() {
        return id;
    }

    public String getCardId() {
        return cardId;
    }

    public int getInterval() {
        return interval;
    }

    public int getRepetitionNumber() {
        return repetitionNumber;
    }

    public void increaseInterval(int inc) {
        interval += inc;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public void increaseRepetitionNumber() {
        this.repetitionNumber++;
    }

    public void setRepetitionNumber(int repetitionNumber) {
        this.repetitionNumber = repetitionNumber;
    }

    public void increaseIntervalByEaseFactor() {
        interval = (int) (interval * easeFactor);
    }

    @Override
    public String toString() {
        return "ReviewData{" +
                "id='" + id + '\'' +
                ", cardId='" + cardId + '\'' +
                ", easeFactor=" + easeFactor +
                ", interval=" + interval +
                ", repetitionNumber=" + repetitionNumber +
                '}';
    }
}
