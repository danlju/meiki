package com.danlju.meiki.core.domain;

import java.util.ArrayList;
import java.util.List;

public class Deck {

    public Deck(String id, String name, User createdByUser, List<Card> cards) {
        this.id = id;
        this.name = name;
        this.createdByUser = createdByUser;
        this.cards = cards;
    }

    public Deck(String id) {
        this.id = id;
    }

    public void setCreatedByUser(User createdByUser) {
        this.createdByUser = createdByUser;
    }

    private String id;

    private String name;

    private User createdByUser;

    private List<Card> cards = new ArrayList<>();

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public User getCreatedByUser() {
        return createdByUser;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void addCard(Card card) {
        this.cards.add(card);
    }

    public void addCards(List<Card> cards) {
        this.cards.addAll(cards);
    }

    @Override
    public String toString() {
        return "Deck{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", createdByUser=" + createdByUser +
                ", cards=" + cards +
                '}';
    }
}